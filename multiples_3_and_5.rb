'''
A code snippet to compute the sum of multiples of 3 and 5 to an upper limit
Creation Date: 07/17/16
Author: Usman Tahir

Project Euler Challenge #1
'''

def mult_3_and_5(limit)
	sum, numbers = 0, (0...limit).to_a
	numbers.to_a.each {
		|number|
		if ((number % 3 == 0) || (number % 5 == 0))
			sum += number
		end
	}
	sum
end

mult_3_and_5(1000)
