# Ruby Snippets #

### Description ###
This repository has been set up to host a miscellany of Ruby Snippets from different projects and ideas. As it becomes updated, you will see different descriptions appear below, that detail the scope/functionality of the project/snippet, as well as any future updates that might be made to it.